<?php
require __DIR__ . "/../vendor/autoload.php";

use Tracy\Debugger;
use Pilulka\Tracy\Elasticsearch\ElasticsearchPanel;

$queries = array();
array_push($queries, add('test1', 32));
array_push($queries, add('test2', 48));
array_push($queries, add('test3', 64));


Debugger::enable(Debugger::DEVELOPMENT);
Debugger::getBar()->addPanel(new ElasticsearchPanel($queries));

function add($q, $t)
{
    return array('statement' => $q, 'time' => $t);
}
