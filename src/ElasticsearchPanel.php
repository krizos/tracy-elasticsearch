<?php
namespace Pilulka\Tracy\Elasticsearch;

use Pilulka\Tracy\Elasticsearch\template\TemplateRender;
use Tracy\IBarPanel;

class ElasticsearchPanel implements IBarPanel
{
    public $data;

    /**
     * EsPanel constructor.
     * @param array $queries
     */
    public function __construct($queries)
    {
        $timeTotal = 0;
        foreach ($queries as $query) {
            $timeTotal += (int)$query['time'];
        }

        $this->data = array_merge(
            array('queries' => $queries),
            array(
                'timeTotal' => $timeTotal,
                'queriesTotal' => count($queries)
            )
        );
    }

    public function getTab()
    {
        return (new TemplateRender(
            __DIR__ . '/template/tab.phtml',
            $this->data
        ))->render();
    }

    public function getPanel()
    {
        ob_start(function () {
        });
        $data = $this->data;
        require __DIR__ . "/panel.phtml";
        return ob_get_clean();
    }
}