<?php
namespace Pilulka\Tracy\Elasticsearch;

interface Template
{

    public function render();

}
